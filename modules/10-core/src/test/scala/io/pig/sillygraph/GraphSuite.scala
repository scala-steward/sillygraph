package io.pig.sillygraph

import munit.FunSuite
import graph.Graph

class GraphSuite extends FunSuite {

  test("can construct graphs from adjacency lists") {
    val g = Graph.fromAdjacentNodes(List("a", "b", "c", "a"))
    assertEquals(g.size, 3)
    assertEquals(g.edges.size, 3)
  }

  test("can construct a graph from a list of nodes and a list of edges") {
    val g = Graph(List("a", "b", "c"))(List((0, 1), (1, 2), (2, 0)))
    assertEquals(g.size, 3)
    assertEquals(g.edges.size, 3)
  }

  test("can construct a graph from a vector of nodes and a vector of edges") {
    val g = Graph(Vector("a", "b", "c"))(Vector((0, 1), (1, 2), (2, 0)))
    assertEquals(g.size, 3)
    assertEquals(g.edges.size, 3)
  }

  test("can construct a graph by filtering nodes of other graph") {
    val g = Graph.fromAdjacentNodes(List("a", "b", "c", "a"))
    assertEquals(g.size, 3)
    assertEquals(g.edges.size, 3)

    val f = g.filterNodes(_ != "b")
    assertEquals(f.size, 2)
    assertEquals(f.edges.size, 1)
  }

  test("can construct a graph by filtering other graph edges by count") {
    val g = Graph.fromAdjacentNodes(List("a", "b", "a", "b", "c", "a"))
    assertEquals(g.size, 3)
    assertEquals(g.edges.size, 5)

    val f = g.filterEdgeCount(_ > 1)
    assertEquals(f.size, 3)
    assertEquals(f.edges.size, 2)
  }

  test("connected nodes are neighbours") {
    val g = Graph.fromAdjacentNodes(List("a", "b", "c", "a"))
    assertEquals(g.neighbours("b").sorted, Vector("a", "c"))
  }

  test("edgeCounts yields counts by edge index") {
    val g        = Graph.fromAdjacentNodes(List("a", "b", "a", "b", "c", "a"))
    val expected = Map((0, 1) -> 2, (1, 0) -> 1, (1, 2) -> 1, (2, 0) -> 1)
    assertEquals(g.edgeCounts.toVector.sorted, expected.toVector.sorted)
  }

  test("edgeCountsWithNodes yields counts with tuples of node values") {
    val g        = Graph.fromAdjacentNodes(List("a", "b", "a", "b", "c", "a"))
    val expected = Map(("a", "b") -> 2, ("b", "a") -> 1, ("b", "c") -> 1, ("c", "a") -> 1)
    assertEquals(g.edgeCountsWithNodes.toVector.sorted, expected.toVector.sorted)
  }

}
